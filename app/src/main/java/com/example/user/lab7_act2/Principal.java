package com.example.user.lab7_act2;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Principal extends AppCompatActivity {
    Button btnCrearFicInt;
    Button btnCrearFichExt;
    Button btnLeerFicInt;
    Button btnLeerFichExt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnCrearFicInt = (Button) findViewById(R.id.button);
        btnCrearFicInt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                escribirFicheroMemInt();
            }
        });
        btnLeerFicInt = (Button) findViewById(R.id.button2);
        btnLeerFicInt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leerFicheroMemInt();
            }
        });
        btnCrearFichExt = (Button) findViewById(R.id.button3);
        btnCrearFichExt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearFicheroMemExt();
            }
        });
        btnLeerFichExt = (Button) findViewById(R.id.button4);
        btnLeerFichExt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leerFicheroMemExt();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void escribirFicheroMemInt(){
        try{
            OutputStreamWriter fout = new OutputStreamWriter(openFileOutput("prueba_int.txt", Context.MODE_PRIVATE));
            fout.write("Texto de prueba.");
            fout.close();
        }
        catch (Exception ex){
            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        }
    }

    public void leerFicheroMemInt(){
        try
        {
            BufferedReader fin =new BufferedReader(new InputStreamReader(openFileInput("prueba_int.txt")));
            String texto = fin.readLine();
            Toast.makeText(this, texto, Toast.LENGTH_LONG).show();
            fin.close();
        }
        catch (Exception ex){
            Log.e("Ficheros", "Error al leer desde memoria interna");
        }
    }

    public void crearFicheroMemExt(){
        try
        {
            File ruta_sd = Environment.getExternalStorageDirectory();
            File f = new File(ruta_sd.getAbsolutePath(), "prueba_sd.txt");
            OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(f));
            fout.write("Texto de prueba externo.");
            fout.close();
        }
        catch (Exception ex){
            Log.d("Ficheros", "Error al escribir fichero a tarjeta SD");
        }
    }

    public void leerFicheroMemExt(){
        try{
            File ruta_sd = Environment.getExternalStorageDirectory();
            File f = new File(ruta_sd.getAbsolutePath(), "prueba_sd.txt");
            BufferedReader fin = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            String texto = fin.readLine();
            Toast.makeText(this, texto, Toast.LENGTH_LONG).show();
            fin.close();
        }
        catch (Exception ex){
            Log.d("Ficheros", "Error al leer fichero desde tarjeta SD");
        }
    }
}
